from attachment_type import AttachmentType


class Attachment:

    def __init__(self, url: str, t: AttachmentType):
        self.url = url
        self.type = t
