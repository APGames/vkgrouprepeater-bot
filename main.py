import logging

from telegram.ext import *
import vk_api.vk_api
from vk_api.bot_longpoll import VkBotLongPoll
from datetime import datetime

from handlers import register_handlers, mail_all
from config import vk_api_token, vk_group_id, telegram_token
from message import Message
from attachment import Attachment
from attachment_type import AttachmentType

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


def main():
    updater = Updater(telegram_token, use_context=True)
    dp = updater.dispatcher
    register_handlers(dp)
    updater.start_polling()

    vk = vk_api.VkApi(token=vk_api_token)
    long_poll = VkBotLongPoll(vk, vk_group_id)

    for post in long_poll.listen():
        message = Message(post.obj["text"])
        if "attachments" in post.obj:
            for attach in post.obj["attachments"]:
                if attach["type"] == "photo":
                    message.add_attachment(Attachment(attach["photo"]["sizes"][-1]["url"], AttachmentType.Photo))
                elif attach["type"] == "video":
                    message.add_attachment(Attachment(attach["video"]["image"][-1]["url"], AttachmentType.Photo))
                elif attach["type"] == "link":
                    message.add_attachment(Attachment(attach["link"]["url"], AttachmentType.Link))
        message.add_attachment(Attachment(
            datetime.utcfromtimestamp(post.obj["date"]).strftime('%Y-%m-%d %H:%M:%S'),
            AttachmentType.Link
        ))
        mail_all(updater.bot, message)


if __name__ == '__main__':
    main()
