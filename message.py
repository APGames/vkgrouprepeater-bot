from typing import List
from attachment import Attachment
from attachment_type import AttachmentType
from telegram import Bot


class Message:

    def __init__(self, text: str, attachments: List[Attachment] = []):
        self.text = text
        self.attachments = attachments

    def add_attachment(self, attachment: Attachment):
        if attachment.type == AttachmentType.Link:
            self.text += "\n\n" + attachment.url
            return
        self.attachments.append(attachment)

    def send(self, bot: Bot, user_id: int):
        for attach in self.attachments:
            if attach.type == AttachmentType.Photo:
                bot.send_photo(user_id, attach.url, caption=self.text)
                return
        bot.send_message(user_id, self.text)
